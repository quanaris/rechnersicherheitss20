#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(){
  char pwd[256];
  int authenticated = 0;

  read(0, pwd, 256);
  if(0 == strcmp(pwd, "password\n")) {
    authenticated = 1;
  }
  else {
    printf(pwd);
  }

  if(authenticated) {
    printf("Access granted.\n");
  }

  return 0;
}
