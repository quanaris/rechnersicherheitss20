#!/bin/python
'''
 Small password hash cracker written in python
 initial version without multi threading
 on one core@3.6 GHz -> ./sha256sum_cracker.py  12.67s user 1.05s system 99% cpu 18.736 total
 
 The passwords in file sha2pwd.txt are hashed with sha256 and a known salt/seed, 
 furthermore the the password policy is know, so that all passwords have a length of 4, with
 the possible upper case, lower case chars and the digits 0-9: Alphabet = {A..Z,a..z,0..9}
 Basic idea: Create a list of all possible permutations if 4 sign long passwords, build a tuple
 with sha256 hashes as keys and password as values. Finally do comparison of the sha2pwd.txt amd build dictionary 
 for matching keys to get the corresponding password.
 
'''

import crypt
import string
from hashlib import sha256
from itertools import product

# creates a dict with all permutations for a password from the alphabet {A..Z,a..z,0..9}
# Warning pretty slow - hence write ops are slow
def create_dict_file(path):
    with open(path, 'w') as file:
        for p in product(string.ascii_letters + string.digits , repeat=4):
            file.write("".join(p) + "\n")

# creates a dict with all permutations of 4 sing long passwords containing
# {A..Z,a..z,0..9}
def create_dict():
    perm_list = []
    for p in product(string.ascii_letters + string.digits , repeat=4):
        perm_list.append("".join(p))
    return perm_list

# take a list of passwords an build a dictionary containing sha256 hash
# with given input in_file: a list of passwords
# out_file: file to write the tuples (sha2hash, pwd)
def create_pwd_sha256_file(in_file, out_file, salt):
    with open(file, 'r') as f:
        pwd_line = f.read().splitlines()
        for pwd in pwd_line:
            pwd_byte = bytes(salt + pwd, 'utf-8')
            hashed = sha256(pwd_byte)
            output = hashed.hexdigest() 
            with open(out_file, 'a') as file:
                file.write(output + ":" + pwd + "\n")

# take a list of passwords an build a dictionary containing sha256 hash
# with given list of passwords (pwd_list) and seed/salt value
# calculate sha256-hash 
# build dictionary -> (key: sha2hash, value: pwd)
def create_pwd_sha256(pwd_list, salt):
    pwd_sha256 = {}
    for pwd in pwd_list:
        h = bytes(salt + pwd, 'utf-8')
        hashed = sha256(h)
        output = hashed.hexdigest()
        pwd_sha256[output] = pwd
    return(pwd_sha256)

# create list of passwords from a given file
# return list containing a list of sha256 hashes
def read_sha2_pwd_file(file):
    ret = []
    with open(file, 'r') as f:
        tmp = f.read().splitlines()
        for t in tmp:
            ret.append(t)
    return ret

# compare a list of hashes with the keys of the 
# dict and print out the matching key value tuples
def compare(hashed, dictionary):
    for h in hashed:
        if h in dictionary:
            print(h, " ", dictionary[h])
    

def main():
    four_perm_sign_list = create_dict()
    salt = 'EArfTx8Pi78221jILzjr'
    pd = create_pwd_sha256(four_perm_sign_list, salt)
    rf = read_sha2_pwd_file('sha2pwd.txt')
    compare(rf,pd)

if __name__ == '__main__':
    main()
