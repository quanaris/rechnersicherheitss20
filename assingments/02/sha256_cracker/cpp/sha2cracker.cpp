#include <bits/stdc++.h> 
#include <iostream>
#include "sha256.h"
#include <iterator> 
#include <fstream>
#include <map> 
#include <vector>
using namespace std;

void printAllKLengthRec(map<string, string> &dict, char set[], string prefix, 
                                    int n, int k) { 
    // Base case: k is 0, 
    // print prefix
    string salted = "EArfTx8Pi78221jILzjr" + prefix;
    string hashed = sha256(salted);
    if (k == 0) {
	
	dict.insert(pair<string, string>(hashed, prefix));
        //cout <<  << endl; 
        return; 
    } 
  
    // One by one add all characters  
    // from set and recursively  
    // call for k equals to k-1 
    for (int i = 0; i < n; i++) 
    { 
        string newPrefix; 
          
        // Next character of input added 
        newPrefix = prefix + set[i]; 
          
        // k is decreased, because  
        // we have added a new character 
        printAllKLengthRec(dict, set, newPrefix, n, k - 1); 
    } 
  
}

void printAllKLength(map<string, string> &dict, char set[], int k,int n)
{
    printAllKLengthRec(dict, set, "", n, k);
}

int main(void) {
    map<string, string> dict;

    char set1[] = {'a', 'b','c','d','e','f','g','h','i','j','k',
	    'l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
	    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S',
	    'T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'}; 
    int k = 4; 
    cout << "[+] generating brute force dict of sha256 key password value" << std::endl;

    printAllKLength(dict, set1, k, 62);
    
    cout << "[+] finished dictionary beginning attack" << std::endl;
    
   string line;
   vector<std::string> h_file;
   ifstream dump("sha2pwd.txt");
   cout << "[+} read file" << std::endl;
   while (std::getline(dump, line)) {
   	h_file.push_back(line);
   }
   cout << "[+] start iterating" << std::endl;
   for (auto i = h_file.begin(); i != h_file.end(); ++i){
	string tmp = *i;
	cout << tmp << "\t" << dict.find(tmp)->second << std::endl;
   }
    return 0;
}
