/*
 *TODO: take care of memory allocations - usage of bzero every piece of data
 *TODO: input sanatization ~ only valid ASCII chars of a-zA-Z0-1
 *TODO: possible printf vuln. as the input is not cleaned
 'TODO: check for environment variables that could be exploited
 *TODO: check all return types, some are size_t or ssize_t not int etc
 *TODO: Plain TCP, no crypto, no authentication
 */

#include"echo_server.h"
#include"aux.c"

int 
main(void) {
	int sockfd, new_sockfd;//fds for socket and every new connection
	struct sockaddr_in host_addr, client_addr;//struct for socket infos
	socklen_t sin_size;//32 bit unsigned for socket
	ssize_t recv_length = 1, yes = 1;
	char buffer[1024];//actual message buffer unsigned char has to be used ---

	//open socket file descriptor
	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		perror("failed in socket");
		exit(EXIT_FAILURE);
	}

	//sockopt for handling socket, as reopening...
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == 1) {
		perror("failed setting socket option SO_REUSEADDR");
		exit(EXIT_FAILURE);
	}

	//TCP/IP infos for socket
	host_addr.sin_family = AF_INET;//Address Family IPv4
	host_addr.sin_port = htons(PORT);//Port defined in echo_server.h
	host_addr.sin_addr.s_addr = INADDR_ANY;//take care of addr
	memset(&(host_addr.sin_zero), '\0', 8);//fill last 8byte with 0

	//actual bind of socket with previous set infos
	if (bind(sockfd, (struct sockaddr *) &host_addr, sizeof(struct sockaddr)) == -1) {
		perror("failed binding socket");
		exit(EXIT_FAILURE);
	}

	//start listening, fst arg socket fd, snd backlog queue for incomplete sockets
	if (listen(sockfd, 5) == -1) {
		perror("failed listening on socket");
		exit(EXIT_FAILURE);
	}
	//Here begins the funny part
	print_ip();//just for the client, is set to any_addr but can be changed
	// server behaviour
	while(1) {//Accpet connection
		bzero(buffer, 1024);
		sin_size = sizeof(struct sockaddr_in);
		new_sockfd = accept(sockfd, (struct sockaddr *) &client_addr, &sin_size);//for each socket accept
		if(new_sockfd == -1) {
			perror("failed accepting connection");
			exit(EXIT_FAILURE);
		}

		printf("server: got connection from %s port %d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
		send(new_sockfd, "Connected to echo server\n", 25, 0);//hello from server
		recv_length = recv(new_sockfd, &buffer, 1024, 0);//receive max 1k buffer
		while(recv_length > 0) {//if bytes receive
			printf("RECV: %ld bytes\n", recv_length);//info for server->"loggin"
			dump(buffer, recv_length);//nice printing with hex
			recv_length = recv(new_sockfd, &buffer, 1024, 0);//length to echo back to client

			printf("Sending back to client %ld bytes\n", recv_length);
			printf("%s\n", buffer);
			send(new_sockfd, &buffer,recv_length,0);//echo message via socket, buffer, length, default TCP PF
		}
		close(new_sockfd);
	}
	return 0;
}

