/*
 * TODO: Client generates overflow with optimization flag set on under ubuntu 18.04 - no tine to dive into!
 * TODO: clean up sanatize user input
 * TODO: bzero + memset
*/
#include"echo_server.h"

int 
main(int argc, char **argv) {
	if (argc < 1) {
	printf("Usage: echo_client IPv4 -- default Port is set to 4242");
        exit(EXIT_FAILURE);
    	}
	int sockfd;
	int recv_length = 1;

	struct sockaddr_in host_addr;
	char buffer[1024], server_rply[1024];

	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		perror("failed in creating socket");
		exit(EXIT_FAILURE);
	}

	memset(buffer, '0',sizeof(buffer));
	memset(&host_addr, '0', sizeof(host_addr));

	host_addr.sin_family = AF_INET;
	host_addr.sin_port = htons(PORT);

	if (inet_pton(AF_INET, argv[1], &host_addr.sin_addr) == -1) {
		perror("inet pton error occured");
		exit(EXIT_FAILURE);
	}
	
	if( connect(sockfd, (struct sockaddr *)&host_addr, sizeof(host_addr)) < 0) {
		perror("failed to connect to server");
		exit(EXIT_FAILURE);
	}

	recv_length = recv(sockfd, server_rply, strlen(server_rply),0);
   	printf("%*.*s", recv_length, recv_length, buffer);
	while(1) {
		memset(&server_rply, '0', sizeof(server_rply));
		fgets(buffer, sizeof(buffer), stdin);
		if( send(sockfd, buffer, strlen(buffer),0) < 0) {
			perror("failed sending message");
			exit(EXIT_FAILURE);
		}

		recv_length = recv(sockfd, server_rply, strlen(server_rply),0);
		printf("%*.*s", recv_length, recv_length, buffer);
	}
	close(sockfd);	

	return 0;
}	
