%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -7 -17 107 37 
%%Creator: MetaPost
%%CreationDate: 2009.06.30:1911
%%Pages: 1
%%DocumentFonts: CMR10 CMMI12
 /cmr10 /CMR10 def
 /cmmi12 /CMMI12 def
/fshow {exch findfont exch scalefont setfont show}bind def
%%EndProlog
%%Page: 1 1
21.88918 5.32462 moveto
(g) cmmi12 11.95517 fshow
 0 0.99626 dtransform truncate idtransform setlinewidth pop [] 0 setdash
 1 setlinecap 0 setlinejoin 10 setmiterlimit
newpath 5.97734 0 moveto
43.83528 0 lineto stroke
newpath 36.47194 -3.05008 moveto
43.83528 0 lineto
36.47194 3.05008 lineto
 closepath
gsave fill grestore stroke
70.95856 3 moveto
(\013) cmmi12 11.95517 fshow
newpath 55.79042 0 moveto
93.64836 0 lineto stroke
newpath 86.28502 -3.05008 moveto
93.64836 0 lineto
86.28502 3.05008 lineto
 closepath
gsave fill grestore stroke
46.0521 31.75964 moveto
(\013) cmmi12 11.95517 fshow
newpath 3.2416 5.02339 moveto
25.98924 36.67162 73.63658 36.67172 96.38437 5.02368 curveto stroke
newpath 89.33765 8.7483 moveto
91.72882 7.68962 94.08289 6.45021 96.38437 5.02368 curveto
95.76572 7.6598 94.9776 10.20076 94.03543 12.64021 curveto
 closepath
gsave fill grestore stroke
newpath 5.97757 0 moveto
5.97757 3.30132 3.30132 5.97757 0 5.97757 curveto
-3.30132 5.97757 -5.97757 3.30132 -5.97757 0 curveto
-5.97757 -3.30132 -3.30132 -5.97757 0 -5.97757 curveto
3.30132 -5.97757 5.97757 -3.30132 5.97757 0 curveto closepath fill
 1 setgray
 0 setgray
newpath 5.97757 0 moveto
5.97757 3.30132 3.30132 5.97757 0 5.97757 curveto
-3.30132 5.97757 -5.97757 3.30132 -5.97757 0 curveto
-5.97757 -3.30132 -3.30132 -5.97757 0 -5.97757 curveto
3.30132 -5.97757 5.97757 -3.30132 5.97757 0 curveto closepath stroke
newpath 55.79065 0 moveto
55.79065 3.30132 53.1144 5.97757 49.81308 5.97757 curveto
46.51176 5.97757 43.83551 3.30132 43.83551 0 curveto
43.83551 -3.30132 46.51176 -5.97757 49.81308 -5.97757 curveto
53.1144 -5.97757 55.79065 -3.30132 55.79065 0 curveto closepath fill
 1 setgray
 0 setgray
newpath 55.79065 0 moveto
55.79065 3.30132 53.1144 5.97757 49.81308 5.97757 curveto
46.51176 5.97757 43.83551 3.30132 43.83551 0 curveto
43.83551 -3.30132 46.51176 -5.97757 49.81308 -5.97757 curveto
53.1144 -5.97757 55.79065 -3.30132 55.79065 0 curveto closepath stroke
 1 setgray
newpath 105.60373 0 moveto
105.60373 3.30132 102.92747 5.97757 99.62616 5.97757 curveto
96.32484 5.97757 93.64859 3.30132 93.64859 0 curveto
93.64859 -3.30132 96.32484 -5.97757 99.62616 -5.97757 curveto
102.92747 -5.97757 105.60373 -3.30132 105.60373 0 curveto closepath fill
 0 setgray
newpath 105.60373 0 moveto
105.60373 3.30132 102.92747 5.97757 99.62616 5.97757 curveto
96.32484 5.97757 93.64859 3.30132 93.64859 0 curveto
93.64859 -3.30132 96.32484 -5.97757 99.62616 -5.97757 curveto
102.92747 -5.97757 105.60373 -3.30132 105.60373 0 curveto closepath stroke
newpath 103.85295 4.22679 moveto
95.39937 -4.22679 lineto stroke
newpath 95.39937 4.22679 moveto
103.85295 -4.22679 lineto stroke
-3.32605 -14.12497 moveto
(x) cmmi12 11.95517 fshow
46.82777 -14.12497 moveto
(z) cmmi12 11.95517 fshow
96.55786 -14.12497 moveto
(y) cmmi12 11.95517 fshow
showpage
%%EOF
