
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "tweetnacl.h"
#include "file.h"
#include "crypto.h"
#include <getopt.h>

#define MAXNAME 255

/* This is what we read from and write to */
int fd;

/* We keep hashes in the BSS rather than on the stack */
hash_t hash;

/* Default names of files we use */
char *file_hash = "exam.hash";
char *file_key  = "exam.key";
char *file_enc  = "exam.enc";
char *file_pdf  = "exam.pdf";
char *answ_zip  = "answers.zip";
char *answ_hash = "answers.hash";


void usage(char *name)
{
  printf("USAGE: %s\n", name);
  printf("  -h this message\n");
  printf("  -1 Create hash of exam\n");
  printf("  -2 Open the exam\n");
  printf("  -3 Create hash of answers\n");
}


int exam_init()
{
  me_t *me;

  me = init_key();

  if (0 > file_savesync(file_key, me, sizeof(me_t))) {
    perror(file_key);
    return -1;
  }
  return 0;
}


int exam_close()
{
  dgram_t *dgram;
  ssize_t ssize;
  size_t size;
  me_t *me;

  me = get_key();

  if (0 > file_loadsync(file_key, me, sizeof(*me))) {
    perror(file_key);
    return -1;
  }
  ssize = file_size(file_pdf);

  if (0 >= ssize) {
    perror(file_pdf);
    return -1;
  }
  size = ((size_t)ssize) + sizeof(dgram_t);
  dgram = (dgram_t *)malloc(size);

  if (NULL == dgram) {
    fprintf(stderr, "Cannot allocate %zu bytes of memory!\n", size);
  }
  if (0 > file_loadsync(file_pdf, dgram->data, size - offsetof(dgram_t, data))) {
    perror(file_pdf);
    return -1;
  }
  dgram_box(hash, dgram, size, me->box_k);

  if (0 > file_savesync(file_enc, dgram, size)) {
    perror(file_enc);
    return -1;
  }
  if (0 > file_savesync(file_hash, hash, sizeof(hash))) {
    perror(file_hash);
    return -1;
  }
  return 1;
}


int exam_open()
{
  dgram_t *dgram;
  uint8_t *data;
  ssize_t ssize;
  size_t size;
  me_t *me;

  me = get_key();

  if (0 > file_loadsync(file_key, me, sizeof(*me))) {
    perror(file_key);
    return -1;
  }
  ssize = file_size(file_enc);

  if (0 >= ssize) {
    perror(file_enc);
    return -1;
  }
  size = (size_t)ssize;
  data = (uint8_t *)malloc(size);

  if (NULL == data) {
    fprintf(stderr, "Cannot allocate %zu bytes of memory!\n", size);
  }
  if (0 > file_loadsync(file_enc, data, size)) {
    perror(file_enc);
    return -1;
  }
  dgram = (dgram_t *)data;

  dgram_open(dgram, size, me->box_k);

  if (0 > file_savesync(file_pdf, dgram->data, size - offsetof(dgram_t, data))) {
    perror(file_pdf);
    return -1;
  }
  return 1;
}


int exam_hash(char *file_in, char *file_out)
{
  uint8_t *data;
  ssize_t ssize;
  size_t size;

  ssize = file_size(file_in);

  if (0 >= ssize) {
    perror(file_in);
    return -1;
  }
  size = (size_t)ssize;
  data = (uint8_t *)malloc(size);

  if (NULL == data) {
    fprintf(stderr, "Cannot allocate %zu bytes of memory!\n", size);
  }
  if (0 > file_loadsync(file_in, data, size)) {
    perror(file_in);
    return -1;
  }
  crypto_hash(hash, data, size);

  if (0 > file_savesync(file_out, hash, sizeof(hash))) {
    perror(file_out);
    return -1;
  }
  show_array(hash, sizeof(hash));
  return 1;
}


int main(int argc, char **argv)
{
  char *name;
  int cmd;
  int ch;

  name = argv[0];
  cmd = 1;

  while ((ch = getopt(argc, argv, "hic123")) != -1) {
    switch (ch)
    {
    case 'i':
      cmd = 0;
      break;

    case '1':
      cmd = 1;
      break;

    case '2':
      cmd = 2;
      break;

    case '3':
      cmd = 3;
      break;

    case 'c':
      cmd = 9;
      break;

    default:
      usage(name);
      exit(0);
    }
  }
  argc -= optind;
  argv += optind;

  if (argc > 0) {
    usage(name);
    exit(0);
  }
  switch(cmd) {
  case 0:
    return exam_init();
    break;

  case 1:
    return exam_hash(file_enc, file_hash);
    break;

  case 2:
    return exam_open();
    break;

  case 3:
    return exam_hash(answ_zip, answ_hash);
    break;

  case 9:
    return exam_close();
    break;

  default:
    fprintf(stderr, "Not implemented!\n");
    break;
  };
  return -1;
}
