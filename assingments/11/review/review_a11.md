Score: 7.5
Comments:

Exercise 1:
3.0 Points
        - First (v) A has an Edge to y, then (iv) x can take that right from
          A.
        - Text and tikz graphics are a bit messy

Exercise 2:
3.0 Points

a)
      - (g) g right to A is enough

b)
      - correct

c)
      - Conspirators is the minumum set of subjects, that are needed such
          that can.share(t,s,p, G0) is true (independently if the subject
          takes or grants a (necessary) right)

Exercise 3:
1.5 Points

        - I assumed before a -> there is a new line, so you wrote for while a > 0:
           -> S := S'
           -> verify (a > 0) <= S
        - See D. Denning. Cryptography and Data Security. section 5.4.2 and 5.4.3
        - Line 1 S' represents all statements in our while body,
          thus we have \underline{a} \oplus \underline{0} = \underline{a}
          \oplus Low = \underline{a} \leq \underline{a} \oplus \underline{b}
          (all targets in the while boby, that have a possible flow = all
          assignments in our while body)
          So here we have \underline{S'} = \underline{a} \oplus \underline{b}
          and \underline{e} = \underline{a} \oplus \underline{0} =
          \underline{a} \oplus Low = \underline{a}  (because 0 is a constant
          and all constant have by definition the class Low)
          Note: verify (a > 0) \leq \underline{S'} is an invalid expression.
        - Line 3 there is no \underline{e}, here we have e = a - x, thus
          we have \underline{a} \oplus \underline{x} \leq \underline{a}
          Note: S_1 := e is not correct, S_1 := a is correct (see Table 5.4
          PDF page 312 for b := e)
       - Line 4 there is no \underline{e}, here we have e = a * y, thus we
          have \underline{a} \oplus \underline{y} \leq \underline{b}.
          Note: S_2 := e is not correct, S_2 := b is correct
       - Line 5 would be correct, iff \underline{S_1} = \underline{a} and
          \underline{S_2} = \underline{b}
 