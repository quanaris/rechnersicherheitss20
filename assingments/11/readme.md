# Readme

Der aktuelle Übungszettel wurde verschlüsselt. Das gleiche Tool wird auch für die Klausur verwendet. Dies ist der Probelauf und wir bitten um Feedback im Forum. 


## Tool

Beim Tool sind die Dateinamen (exam.enc, answers.pdf usw.) festgewählt. Das Tool ist ein C-Programm ohne weitere Abhängigkeiten und wird über die Kommandozeile ausgeführt.

### Optionen vom Tool


Option -1, Erzeugt exam.hash wie oben aus exam.enc und gibt den hash aus

Option -2, Erzeugt exam.pdf aus exam.enc und exam.key

Option -3, Erzeugt answers.hash aus answers.zip und gibt den hash aus


## Aufgabe

1. Ausführung der Make-File
2. Erzeugung vom Hash von exam.enc mittels Option 1 und Vergleich des Hashes mit dem mitgelieferten Hash.
3. Entschlüsselung von exam.enc mittels exam.key. Die Dateien müssen noch in den entsprechenden Ordner kopiert werden. Dies erfolgt mit Option 2.
4. Erzeugung des Hashes mit Option 3 für ihre Antwort. Dieser Hash muss nicht hochgeladen werden, aber dennoch sollte der Ablauf geübt werden.