
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/* There should be a syscall for this. */

static int fd = -1;


void randombytes(uint8_t *buf, size_t size)
{
  ssize_t si;
  size_t i;

  if (0 > fd) {
    for (;;) {
      fd = open("/dev/urandom", O_RDONLY);

      if (0 <= fd) {
	break;
      }
      sleep(1);
    }
  }
  while (size > 0) {
    if (size < 1048576) {
      i = size;
    }
    else {
      i = 1048576;
    }
    si = read(fd, buf, i);

    if (1 > si) {
      sleep(1);
      continue;
    }
    i = (size_t)si;
    buf += i;
    size -= i;
  }
}
