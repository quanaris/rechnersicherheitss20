#!/usr/bin/env python3

import numpy as np
import random

BOTTOM = "B"

class Markov(object):

    def __init__(self, data, order):
        self.pins = list(data)
        # add B as bottom symbol
        for i in range(len(self.pins)):
            self.pins[i] = BOTTOM * order + self.pins[i]

        self.order = order
        #get all symbols
        self.symbols = ""
        for i in range(len(self.pins)):
            for j in range(len(self.pins[i])):
                if not self.pins[i][j] in self.symbols:
                    self.symbols = self.symbols + self.pins[i][j]

        #First state is 0 which is equal to order length of Bottoms
        self.state = 0

        #Calculating the Probability Matrix
        self.probability_matrix = np.zeros((len(self.symbols) ** order, len(self.symbols) ** order))

        for i in range(len(self.pins)):
            for j in range(len(self.pins[i]) - order):
                self.probability_matrix[ self.strtonum(self.pins[i],j) ][ self.strtonum(self.pins[i], j+1) ] += 1
        for i in range(len(self.probability_matrix)):
            if np.sum(self.probability_matrix[i]) != 0:
                self.probability_matrix[i] /= np.sum(self.probability_matrix[i])

    def strtonum(self,str,pos):
        num = 0
        for i in range(self.order):
            num += self.symbols.find(str[pos+i]) * len(self.symbols)**i
        return num

    def numtostr(self,num):
        s = ""
        for i in range(self.order):
            s = s + self.symbols[num % len(self.symbols)]
            num = int(num / len(self.symbols))
        return s

    def randomStateTransition(self):
        r = random.random()
        x = 0.0
        for i in range(len(self.probability_matrix[self.state])):
            x += self.probability_matrix[self.state][i]
            if x >= r:
                self.state = i
                return self.numtostr(i)

    def printProbabilityMatrix(self):
        index_s = []
        for i in range(len(self.probability_matrix)):
            index_s += [self.numtostr(i)]
        print(index_s)
        for i in range(len(self.probability_matrix)):
            x_str = np.array_repr(self.probability_matrix[i]).replace('\n', '')
            print(self.numtostr(i) + " : " + str(x_str) )

    def getProbability(self, states):
        p = 1
        p *= self.probability_matrix[0][self.strtonum(states[0],0)]
        for i in range(len(states)-1):
            p *= self.probability_matrix[self.strtonum(states[i],0)][self.strtonum(states[i+1],0)]
        return p

example = [ "1331", "2303", "1301", "2320", "1312", "1330", "1203", "1033", "2332", "2323" ]

print("Creating Model with order 1")
model = Markov(example, 1)
print("Probability of state-transitions in matrix form looks like this:")
model.printProbabilityMatrix()
print("Doing 4 random state-transitions:")
code = ""
states = []
for i in range(4):
    state = str(model.randomStateTransition())
    states += [state]
    code += state
    print("New State:" + state)
print("Code: " + code)
print("Probability: " + str(model.getProbability(states)))


print("Creating Model with order 2")
model = Markov(example, 2)
print("Probability of state-transitions in matrix form looks like this:")
model.printProbabilityMatrix()
print("Doing 4 random state-transitions:")
code = ""
states = []
for i in range(4):
    state = str(model.randomStateTransition())
    states += [state]
    code += state[1]
    print("New State:" + state)
print("Code: " + code)
print("Probability: " + str(model.getProbability(states)))
