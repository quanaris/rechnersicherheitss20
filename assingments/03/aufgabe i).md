The α-guesswork metric G α combines λ β and μ α :
G α (X ) = (1 − λ μ α ) · μ α +
μ α
∑
i · Pr[X = x i ]
i=1
G α measures the expected effort per user to achieve success rate α.