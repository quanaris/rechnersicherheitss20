
/* CHANGES:
 *
 * 2015/04/02 Changed return values from 1 to 0 for no errors.
 * 2015/04/02 Added function to determine file size.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include "file.h"


int file_readsync(int fd, void *buf, size_t len)
{
  uint8_t *u8p;
  ssize_t sin;
  size_t in;
  int flags;

  flags = fcntl(fd, F_GETFL);

  if (0 > flags) {
    return -1;
  }
  if (fcntl(fd, F_SETFL, flags & ~O_NONBLOCK) == -1) {
    return -1;
  }
  u8p = (uint8_t *)buf;

  while (0 < len) {
    sin = read(fd, u8p, len);

    if (0 == sin) {
      break;
    }
    if (-1 == sin) {
      if ((EINTR == errno) || (EAGAIN == errno)) {
	continue;
      }
      break;
    }
    in   = (size_t)sin;
    u8p += in;
    len -= in;
  }
  if (fcntl(fd, F_SETFL, flags) == -1) {
    return -1;
  }
  if (0 != len) {
    return -1;
  }
  return 0;
}


int file_writesync(int fd, void *buf, size_t len)
{
  uint8_t *u8p;
  ssize_t sout;
  size_t out;
  int flags;

  flags = fcntl(fd, F_GETFL);

  if (-1 == flags) {
    return -1;
  }
  if (fcntl(fd, F_SETFL, flags & ~O_NONBLOCK) == -1) {
    return -2;
  }
  u8p = (uint8_t *)buf;

  while (0 < len) {
    sout = write(fd, u8p, len);

    if (-1 == sout) {
      if ((EINTR == errno) || (EAGAIN == errno)) {
	continue;
      }
      break;
    }
    out  = (size_t)sout;
    u8p += out;
    len -= out;
  }
  if (fcntl(fd, F_SETFL, flags) == -1) {
    return -1;
  }
  if (0 != len) {
    return -1;
  }
  return 0;
}


int file_loadsync(const char *name, void *buf, size_t len)
{
  int err;
  int fd;

  fd = open(name, O_RDONLY);

  if (0 > fd) {
    return -1;
  }
  err = file_readsync(fd, buf, len);

  if (close(fd) < 0) {
    return -1;
  }
  return err;
}


int file_savesync(const char *name, void *buf, size_t len)
{
  int err;
  int fd;

  fd = open(name, O_CREAT | O_TRUNC | O_WRONLY, 0644);

  if (0 > fd) {
    return -1;
  }
  err = file_writesync(fd, buf, len);

  if (close(fd) < 0) {
    return -1;
  }
  return err;
}


ssize_t file_size(char *name)
{
  struct stat buf;
  int err;
  int fd;

  fd = open(name, O_RDONLY);

  if (0 > fd) {
    return -1;
  }
  err = fstat(fd, &buf);

  if (close(fd) < 0) {
    return -1;
  }
  if (0 > err) {
    return -1;
  }
  return buf.st_size;
}
