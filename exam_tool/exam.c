
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "tweetnacl.h"
#include "file.h"
#include "crypto.h"

/* Conversion buffer for key */
char conv[2 * sizeof(box_k_t) + 2];

/* We keep hashes in the BSS rather than on the stack */
hash_t hash;

/* Default names of files we use */
char *file_hash = "exam.hash";
char *file_key  = "exam.key";
char *file_enc  = "exam.enc";
char *file_pdf  = "exam.pdf";
char *answ_zip  = "answers.zip";
char *answ_hash = "answers.hash";

/* Hex encoding */
const char *hex = "0123456789abcdef";


void usage(char *name)
{
  printf("USAGE: %s\n", name);
  printf("  -h this message\n");
  printf("  -1 Create hash of exam\n");
  printf("  -2 Open the exam\n");
  printf("  -3 Create hash of answers\n");
  printf("\n");
  printf("USAGE (for instructors): %s\n", name);
  printf("  -i Initialize key\n");
  printf("  -c Close the exam\n");
}


me_t *load_key()
{
  size_t i;
  size_t j;
  me_t *me;
  char c1;
  char c2;

  me = get_key();

  if (0 > file_loadsync(file_key, conv, sizeof(conv))) {
    perror(file_key);
    exit(-1);
  }
  for (i=0, j=0; i < sizeof(box_k_t); i+=1) {
    c1 = (conv[j] < 'a') ? conv[j] - '0' : conv[j] - 'a' + 10;
    j += 1;
    c2 = (conv[j] < 'a') ? conv[j] - '0' : conv[j] - 'a' + 10;
    j += 1;
    me->box_k[i] = (c1 << 4) + (c2 & 15);
  }
  return me;
}


int exam_init()
{
  size_t i;
  size_t j;
  me_t *me;

  me = init_key();

  for (i=0, j=0; i < sizeof(box_k_t); i+=1) {
    conv[j++] = hex[me->box_k[i] >> 4];
    conv[j++] = hex[me->box_k[i] & 15];
  }
  conv[j++] = '\n';
  conv[j++] = '\n';

  if (0 > file_savesync(file_key, conv, j)) {
    perror(file_key);
    return -1;
  }
  return 1;
}


int exam_close()
{
  dgram_t *dgram;
  ssize_t ssize;
  size_t size;
  me_t *me;

  me = load_key();
  ssize = file_size(file_pdf);

  if (0 >= ssize) {
    perror(file_pdf);
    return -1;
  }
  size = ((size_t)ssize) + sizeof(dgram_t);
  dgram = (dgram_t *)malloc(size);

  if (NULL == dgram) {
    fprintf(stderr, "Cannot allocate %zu bytes of memory!\n", size);
    return -1;
  }
  if (0 > file_loadsync(file_pdf, dgram->data, size - offsetof(dgram_t, data))) {
    perror(file_pdf);
    return -1;
  }
  dgram_box(hash, dgram, size, me->box_k);

  if (0 > file_savesync(file_enc, dgram, size)) {
    perror(file_enc);
    return -1;
  }
  if (0 > file_savesync(file_hash, hash, sizeof(hash))) {
    perror(file_hash);
    return -1;
  }
  return 1;
}


int exam_open()
{
  dgram_t *dgram;
  uint8_t *data;
  ssize_t ssize;
  size_t size;
  me_t *me;

  me = load_key();
  ssize = file_size(file_enc);

  if (0 >= ssize) {
    perror(file_enc);
    return -1;
  }
  size = (size_t)ssize;
  data = (uint8_t *)malloc(size);

  if (NULL == data) {
    fprintf(stderr, "Cannot allocate %zu bytes of memory!\n", size);
    return -1;
  }
  if (0 > file_loadsync(file_enc, data, size)) {
    perror(file_enc);
    return -1;
  }
  dgram = (dgram_t *)data;

  dgram_open(dgram, size, me->box_k);

  if (0 > file_savesync(file_pdf, dgram->data, size - offsetof(dgram_t, data))) {
    perror(file_pdf);
    return -1;
  }
  return 1;
}


int exam_hash(char *file_in, char *file_out)
{
  uint8_t *data;
  ssize_t ssize;
  size_t size;

  ssize = file_size(file_in);

  if (0 >= ssize) {
    perror(file_in);
    return -1;
  }
  size = (size_t)ssize;
  data = (uint8_t *)malloc(size);

  if (NULL == data) {
    fprintf(stderr, "Cannot allocate %zu bytes of memory!\n", size);
    return -1;
  }
  if (0 > file_loadsync(file_in, data, size)) {
    perror(file_in);
    return -1;
  }
  crypto_hash(hash, data, size);

  if (0 > file_savesync(file_out, hash, sizeof(hash))) {
    perror(file_out);
    return -1;
  }
  show_array(hash, sizeof(hash));
  return 1;
}


int main(int argc, char **argv)
{
  char *name;
  char *s;

  name = argv[0];

  if (2 != argc) {
    usage(name);
    return 0;
  }
  s = argv[1];

  if ((2 != strnlen(s, 3)) || ('-' != *s)) {
    usage(name);
    return 0;
  }
  switch (s[1])
  {
  case 'i':
    return exam_init();

  case '1':
    return exam_hash(file_enc, file_hash);

  case '2':
    return exam_open();

  case '3':
    return exam_hash(answ_zip, answ_hash);

  case 'c':
    return exam_close();

  default:
    usage(name);
    return 0;
  }
  return -1;
}
