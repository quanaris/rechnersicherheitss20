
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "crypto.h"


me_t me;


me_t *get_key()
{
  return &me;
}


me_t *init_key()
{
  randombytes(me.box_k, sizeof(me.box_k));
  return &me;
}


void show_array(uint8_t *a, size_t size)
{
  size_t i;

  for (i=0; i < size; i++) {
    fprintf(stderr, " %.2x", a[i]);

    if (i % 16 == 15) {
      fprintf(stderr, "\n");
    }
  }
  fprintf(stderr, "\n");
}


int compare_arrays(uint8_t *a, uint8_t *b, size_t size)
{
  int err;

  for (err = 0; 0 < size; size -= 1) {
    err = err | (*a != *b);
    a++;
    b++;
  }
  return err ? -1 : 0;
}


void dgram_box(hash_t h, dgram_t *p, size_t size, box_k_t k)
{
  size_t len;
  cca_t cca;

  if (sizeof(dgram_t) > size) {
    exit(-1);
  }
  len = size - sizeof(dgram_t);

  /* Randomize nonce */
  randombytes(p->nonce, sizeof(p->nonce));

  /* Generate keys for onetimeauth and stream */
  crypto_stream(cca.bytes, sizeof(cca), p->nonce, k);
  
  /* Encrypt and authenticate */
  crypto_stream_xor(p->data, p->data, len, p->nonce, cca.stream_k);
  crypto_onetimeauth(p->auth, p->data, len, cca.auth_k);

  /* Purge keys */
  memset(cca.bytes, 0, sizeof(cca));

  /* Compute message hash */
  crypto_hash(h, p->bytes, size);
}


int dgram_open(dgram_t *p, size_t size, box_k_t k)
{
  size_t len;
  cca_t cca;

  if (sizeof(dgram_t) > size) {
    exit(-1);
  }
  len = size - sizeof(dgram_t);

  /* Generate keys for onetimeauth and stream */
  crypto_stream(cca.bytes, sizeof(cca), p->nonce, k);
  
  /* Verify one-time authenticator */
  if (0 > crypto_onetimeauth_verify(p->auth, p->data, len, cca.auth_k)) {
    return -1;
  }
  /* Decrypt */
  crypto_stream_xor(p->data, p->data, len, p->nonce, cca.stream_k);
  
  /* Purge keys */
  memset(cca.bytes, 0, sizeof(cca));
  return 0;
}
