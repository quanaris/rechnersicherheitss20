#ifndef FILE_H
#define FILE_H

#include <stdint.h>

int file_readsync(int fd, void *buf, size_t len);
int file_writesync(int fd, void *buf, size_t len);
int file_loadsync(const char *name, void *buf, size_t len);
int file_savesync(const char *name, void *buf, size_t len);
ssize_t file_size(char *name);

#endif
