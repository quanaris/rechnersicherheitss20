
/* Use cc -std=c11 to accept anonymous structs and unions! */

#ifndef CRYPTO_H
#define CRYPTO_H

#include <stdint.h>
#include <stddef.h>
#include "tweetnacl.h"
#include "devurandom.h"

typedef uint8_t stream_k_t [crypto_stream_KEYBYTES];
typedef uint8_t auth_k_t   [crypto_onetimeauth_KEYBYTES];
typedef uint8_t auth_t     [crypto_onetimeauth_BYTES];
typedef uint8_t nonce_t    [crypto_box_NONCEBYTES];
typedef uint8_t box_k_t    [crypto_box_BEFORENMBYTES];
typedef uint8_t hash_t     [crypto_hash_BYTES];

/* Structs used to compute pseudorandom things */

typedef struct me_t me_t;

struct me_t {
  box_k_t box_k;
};

typedef struct cca_t cca_t;

struct cca_t {
  union {
    struct {
      auth_k_t auth_k;
      stream_k_t stream_k;
    };
    uint8_t bytes[1];
  };
};

/* Structs used to operate on packets */

typedef struct dgram_t dgram_t;

struct dgram_t {
  union {
    struct {
      nonce_t nonce;
      auth_t  auth;
      uint8_t data[];
    };
    const uint8_t bytes[1];
  };
};

/* For old-school debugging */
void show_array(uint8_t *a, size_t size);

/* Utility functions */
int compare_arrays(uint8_t *a, uint8_t *b, size_t size);

/* Bulk encryption */
void dgram_box(hash_t h, dgram_t *p, size_t size, box_k_t k);
int dgram_open(dgram_t *p, size_t size, box_k_t k);

/* The all important key */
me_t *get_key();
me_t *init_key();

#endif

