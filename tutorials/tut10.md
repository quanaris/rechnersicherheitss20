Ankündigungen:

    Entschlüsselung vom Aufgabenzettel/Klausur

    Weiterarbeiten am Projekt. Es gibt noch einige Projekte ohne ein Issue! Wer kein Issue anlegt, sollte mir das genau erklären. Schwer vorstellbar! Andere können auch in anderen Repos Issues eröffnen.

    Can share, can steal: Vgl 4.13 in Dennning, Seite 270 

    "We now turn to the question of stealing. Intuitively, a node p steals a right r for x from an owner s if it acquires r for x without the explicit cooperation of s. "

Gruppenarbeit:

    Nachbesprechung (zügig)

    Vorbesprechung und Wiederholung von Take-Grant-Model

    Wie sind Subjekte/Objekte im Graph gekennzeichnet?

    Schwarz/Grau/Weiß

    Was sind die unterschidlichen Kanten? 

    Was ist Alpha? Teilmenge der Rechtemenge

Seit V = {x, y, z, o} mit o ist ein Objekt und E = {(x takes from y), (y takes form z), (z alpha to o)}. Wie kann x alpha auf o erlangen?

    Infoflow: Ab Seite 279

    Seite 279 bzw. 265 lesen: 

    Was wollen wir hier erreichen? Bezug zu Security Policy? Vielleicht auch eine Abgrenzung zu Access Control

    Was ist die Relation? -> Security Class --  Es gibt mehr als kleiner,gleich. 

     Sprung zu 5.4 Compiler-Based Mechanism (bzw. 5.4.3)

    Was zeigt die Tabelle auf Seite 312?

    Hinweis zur Übung: Ab und zu reicht es aus einfach nur verify x < y zu schreiben. Wir können das nicht wirklich überprüfen. Warum nicht?

    Bibifi-Projekt: Diskussion, Fragen. Stellt eure Angriffe vor!

    Falls dann noch Zeit ist: 4.2.3 Design Principles (Seite 220 bzw. 206)

    Lesen

     Beispiele für Umsetzung oder fehlende Umsetzung

     Was ist umsetzbar? Wo sind Konflikte vorprogrammiert und mit wem?

    Wo gibt es Widersprüche zwischen den Prinzipien?


