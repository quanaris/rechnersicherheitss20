4. Tutorium

# Ankündigungen:

* Noten von Übung 1 zusammengetragen, aber noch nicht im KVV
* Projektstart (Siehe Übungszettel, Material im KVV)
* Einfachere Docker-File (Ubuntu mit GCC) -> Alternative eine VM?


# Im Tutorium

* Nachbesprechung von Übung 3
* Diskussion ist es ethisch vertretbar diese Daten zu verwenden? Wie empfandet ihr es?
* Diskussion über Passwortmodell mit Markov-Ketten & Vorbesprechung
* Projektbesprechung
## Nachbesprechung von Übung 3

* Erfahrungsaustausch der verwendeten Tools
* Was fällt bei dem Datensatz auf? Erfüllt es eure Erwartungen an realen Passwörtern?
* Unbedingt Aufgabe e, g, h,i besprechen 
* Vergleich der Ergebnisse: g,h,i
* Erstellt eine Sammlung an Passwort Policies. Erstellt eine Rangfolge der Policies nach euer subjktive Sicherheits-Einschätzung. Erstellt anschließend eine Tabelle mit den Ergebnissen. Gibt es Übereinstimmungen? 


## Passwortmodell mit Markov-Ketten

* Welche Ziele verfolgen wir mit der Erstellung von Passwortmodellen?
- Passwortstärke quantifizieren
- Passwörter generieren
* Woher kennt ihr Markov-Ketten? Weitere Beispiele: https://en.wikipedia.org/wiki/Examples_of_Markov_chains
* Warum ist es sinnvoll Markov-Ketten zu verwenden?
* Warum brauchen wir zwei zusätzliche Zeichen?
* Sei PW = {a,b, aa, c, ab} -> Was ist das Model (1. bzw. 2. Ordnung)?
Pr [a|_]=3/5
Pr [b|_]=1/5
Pr [c|_]=1/5
Pr [a|a]=1/3
Pr [b|a]=1/3
Pr [T|a]=1/3
...
Pr [T|T]=1



## Projektbesprechung

* Testfall zum Erzeugen von Bank/Account erstellen.
* Gemeinsam mit Oliver können wir euer Ergebnis vom Testfall mit einer Referenzimplementierung vergleichen.
* Fragen sammeln
* Vorbereitung zum Threat model spiel (nächste Woche):
        * https://www.youtube.com/watch?v=gZh5acJuNVg
        * https://github.com/adamshostack/eop
        * Grafik erstellen

## Artem
4. Tutorium

# Ankündigungen:

* Noten von Übung 1 zusammengetragen, aber noch nicht im KVV
* Projektstart (Siehe Übungszettel, Material im KVV)
* Einfachere Docker-File (Ubuntu mit GCC) -> Alternative eine VM?


# Im Tutorium

* Nachbesprechung von Übung 3
* Diskussion ist es ethisch vertretbar diese Daten zu verwenden? Wie empfandet ihr es?
* Diskussion über Passwortmodell mit Markov-Ketten & Vorbesprechung
* Projektbesprechung

## Nachbesprechung von Übung 3

* Erfahrungsaustausch der verwendeten Tools
R und Python, 
* Was fällt bei dem Datensatz auf? Erfüllt es eure Erwartungen an realen Passwörtern?
Passwörter mit " am Anfang und Ende, z.b. password kommt 2 mal vor. 
* Unbedingt Aufgabe e, g, h,i besprechen
Einmal mit extenden und 1/gesamt verteilung und einmal mit echten datensatz und deren verteilung
* Vergleich der Ergebnisse: g,h,i

* Erstellt eine Sammlung an Passwort Policies. Erstellt eine Rangfolge der Policies nach euer subjktive Sicherheits-Einschätzung. Erstellt anschließend eine Tabelle mit den Ergebnissen. Gibt es Übereinstimmungen? 
Großbuchtsaben/Zahl/Sonderzeichen/mind. 10 Buchstaben ; 
zwischen 10 und 32/string[-1,-1] groß und klein buchstaben ; 15,33
sonderzeichen/großbuchstaben und 8-32 ; 190



## Passwortmodell mit Markov-Ketten

* Welche Ziele verfolgen wir mit der Erstellung von Passwortmodellen?
Wir wollen 

* Woher kennt ihr Markov-Ketten? Weitere Beispiele: https://en.wikipedia.org/wiki/Examples_of_Markov_chains
* Warum ist es sinnvoll Markov-Ketten zu verwenden?
* Warum brauchen wir zwei zusätzliche Zeichen?
* Sei PW = {a,b, aa, c, ab} -> Was ist das Model (1. bzw. 2. Ordnung)?


        T     a      b    c     -T         a   12/40
a     3/5   1/4   /     /       /           b  8/40
b    1/5   1/4    /     /        /          aa 3/40
c   1/5    /         /    /        /          c    8/20
-T  /        1/2   1    1      1          ab   6/40

## Projektbesprechung

* Testfall zum Erzeugen von Bank/Account erstellen.
* Gemeinsam mit Oliver können wir euer Ergebnis vom Testfall mit einer Referenzimplementierung vergleichen.
* Fragen sammeln
* Vorbereitung zum Threat model spiel (nächste Woche):
        * https://www.youtube.com/watch?v=gZh5acJuNVg
        * https://github.com/adamshostack/eop
        * Grafik erstellen


