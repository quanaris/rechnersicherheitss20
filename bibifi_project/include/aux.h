#ifndef ECHO_SERVER
#define ECHO_SERVER

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<strings.h>
#include<sys/socket.h>
#include <ifaddrs.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/types.h>
#include<unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/if.h>

#define PORT 4242

void 
dump(const unsigned char*, const unsigned int);

void 
print_ip(void);

#endif

