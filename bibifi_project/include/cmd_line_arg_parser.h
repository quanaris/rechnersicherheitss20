#ifndef CMD_LINE_ARG_PARSER
#define CMD_LINE_ARG_PARSER

#include<getopt.h>


typedef enum 
{
	BANK = 0,
	ATM =1
} clap_role_t;

typedef enum 
{
	CREATE_NEW,
	DEPOSIT,
	WITHDRAW,
	GET
} clap_mode_of_op_t;

typedef struct clap_arg_s 
{
	const char *authfile;	
	const char *cardfile;
	const char *ip;
	in_port_t port;
	clap_mode_of_op_t mode;  /* WARNING: not const */
	const char *amount;
	const char *account;
} clap_arg_t;


int
clap_handler(clap_role_t role, int argc, char **argv, clap_arg_t * parsed_args);

#endif
