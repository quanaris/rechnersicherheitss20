#include "../include/cmd_line_arg_parser.h"
#include "vrfy_values.c"
#include <unistd.h> 
#include <stdio.h>
#include <stdbool.h>

static const char *clap_bank_opt_str = "p:s:h?";
static const char *clap_atm_opt_str = "s:i:p:c:n:d:w:ga:h?";
/* Things to consider:
 * + data struct for a port
 * + data struct for an authfile
 * + cJSON as JSON library
 * + validate the input stuff trhough command line
 * + split arg struct in multiple ones and cast in between?
 * + fix maximum memory size of struct?
 */

typedef struct clap_flags_s {
	bool authfile;
	bool ip;
	bool port;
	bool cardfile;
	bool create_new;
	bool deposit;
	bool withdraw;
	bool get;
	bool account;
} clap_flags_t;

static clap_flags_t clap_flags = {
	.authfile=true,
	.ip=true,
	.port=true,
	.cardfile=true,
	.create_new=true,
	.deposit=true,
	.withdraw=true,
	.get=true,
	.account=true,
};

static void
clap_atm_display_usage(void)
{
	fputs(
		"ATM - Rechnersicherheit Projekt 2020\n"
		"\n"
		"Usage: atm [option] MODE ACCOUNT	run atm client\n"
		"\n"
		"Required argument ACCOUNT:\n"
		"  -a <account>		set account name to <account>\n"
		"\n"
		"Required mode of operation MODE:\n"
		"  -n <amount>		create new account with amount set to <amount>\n"
		"  -d <amount>		deposit amount <amount>\n"
		"  -w <amount>		withdraw amount <amount>\n"
		"  -g			get current account's balance\n"
		"\n"
		"Options:\n"
		"  -s <auth-file>	authenticate using <auth-file>\n"
		"  -i <ip_address>	connect to bank's ip <ip-address>\n"
		"  -p <port>		connect to bank's port <port>\n"
		"  -c <card-file>	use card file <card-file>\n",
		stderr);
}

static void
clap_bank_display_usage(void)
{
	fputs(
		"Bank - Rechnersicherheit Projekt 2020\n"
		"\n"
		"Usage: bank [option]		startup bank server\n"
		"\n"
		"Options:\n"
		"  -p <port>		set listening port to <port>\n"
		"  -s <auth-file>	set name of auth-file to <auth-file>\n",
		stderr);
}

static int 
clap_bank_getopt(int argc, char **argv, clap_arg_t * parsed_args) 
{

	static int opt = 0;

	opt = getopt(argc, argv, clap_bank_opt_str);
	while (opt != -1) {
		switch(opt) {
			case 'p': 
				if (clap_flags.port) {
					int val = extract_port(optarg);
					if(val == -1) {
						perror("INVALID PORT NUMBER");
						goto error;
					}
					clap_flags.port=false;
					parsed_args->port = val;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 's':
				if (clap_flags.authfile) {
					char *val = extract_file_name(optarg);
					if(val == -1) {
						perror("INVALID FILE NAME");
						goto error;
					}

					clap_flags.authfile=false;
					parsed_args->authfile = val;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'h':
			case '?':
				goto error;
			default:
				break;
		}

		opt = getopt(argc, argv, clap_bank_opt_str);
	}
	return 0;

error:
	clap_bank_display_usage();
	return 255;
}

static int 
clap_atm_getopt(int argc, char **argv, clap_arg_t * parsed_args) 
{

	static int opt = 0;

	opt = getopt(argc, argv, clap_atm_opt_str);
	while (opt != -1) {
		switch(opt) {
			case 's':
				if (clap_flags.authfile) {
					char *val = extract_file_name(optarg);
					if(val == -1) {
						perror("INVALID FILE NAME");
						goto error;
					}

					clap_flags.authfile=false;
					parsed_args->authfile = val;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'i':
				if (clap_flags.ip) {
					char *val = extract_ip_address(optarg);
					if(val == -1) {
						perror("INVALID IP ADDRESS");
						goto error;
					}

					clap_flags.ip=false;
					parsed_args->ip = val;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'p': 
				if (clap_flags.port) {
					char *val = extract_port(optarg);
					if(val == -1) {
						perror("INVALID PORT NUMBER");
						goto error;
					}
					clap_flags.port=false;
					parsed_args->port = val;
                    printf("%s\n", optarg);
					printf("%d\n", parsed_args->port);
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'c': 
				if (clap_flags.cardfile) {
					clap_flags.cardfile=false;
					parsed_args->cardfile = optarg;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'n': 
				if (clap_flags.create_new && clap_flags.deposit && clap_flags.withdraw && clap_flags.get) {
					clap_flags.create_new=false;
					parsed_args->mode = CREATE_NEW;
					parsed_args->amount = optarg;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'd': 
				if (clap_flags.create_new && clap_flags.deposit && clap_flags.withdraw && clap_flags.get) {
					clap_flags.deposit=false;
					parsed_args->mode = DEPOSIT;
					parsed_args->amount = optarg;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'w': 
				if (clap_flags.create_new && clap_flags.deposit && clap_flags.withdraw && clap_flags.get) {
					clap_flags.withdraw=false;
					parsed_args->mode = WITHDRAW;
					parsed_args->amount = optarg;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'g': 
				if (clap_flags.create_new && clap_flags.deposit && clap_flags.withdraw && clap_flags.get) {
					clap_flags.get=false;
					parsed_args->mode = GET;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'a': 
				if (clap_flags.account) {
					char *val = extract_account_name(optarg);
					if(val == -1) {
						perror("INVALID ACCOUNT NAME");
						goto error;
					}
					clap_flags.account=false;
					parsed_args->account = val;
					break;
				} else {
					//same Argument provided more than once
					goto error;
				}
			case 'h':
			case '?':
				goto error;
			default:
				break;
		}

		opt = getopt(argc, argv, clap_atm_opt_str);
	}
	/* check if required argument -a <account> was provided */
	if(clap_flags.account) goto error;
	
	/* check if card file was provided, if not default to "<account>.card" */
	if(clap_flags.cardfile) {
		/* TODO: did not include ".card" */ 
		parsed_args->cardfile = parsed_args->account;
	}

	return 0;

error:
	clap_atm_display_usage();
	return 1;
}


int
clap_handler(clap_role_t role, int argc, char **argv, clap_arg_t * parsed_args) 
{
	switch(role) {
		case BANK:
			if(clap_bank_getopt(argc, argv, parsed_args)) goto error;	
			break;
		case ATM:
			if(clap_atm_getopt(argc, argv, parsed_args)) goto error;
			break;
		default:
			goto error;	
	}
	return 0;
error:
	return 1;
}
