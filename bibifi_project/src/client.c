#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

#include<sodium.h>
#include "../include/cmd_line_arg_parser.h"
#include "cmd_line_arg_parser.c"

#define BUFFER_SIZE 1024
#define EXIT_STRING "EXIT_SOCKET_CODE"

#define MESSAGE_LEN 1024
#define CIPHERTEXT_LEN (crypto_box_MACBYTES + MESSAGE_LEN)


size_t parseResponse(unsigned char *response);

int main(int argc, char **argv) {

    unsigned char client_publickey[crypto_box_PUBLICKEYBYTES];
    unsigned char client_secretkey[crypto_box_SECRETKEYBYTES];
    crypto_box_keypair(client_publickey, client_secretkey);
    unsigned char server_publickey[crypto_box_PUBLICKEYBYTES];
    unsigned char nonce[crypto_box_NONCEBYTES];

    unsigned char ciphertext[CIPHERTEXT_LEN];
    unsigned char plaintext[MESSAGE_LEN];

	struct timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    FILE *rd_ptr;
    clap_arg_t *parsed_args = &(clap_arg_t) {
            .ip = "127.0.0.1",
            .port = 3000,
            .authfile = "bank.auth",
    };

    /* Command-Line-Argument-Parser (CLAP) */
    if (clap_handler(ATM, argc, argv, parsed_args)) {
        perror("FAILED AT PARSING");
        exit(255);
    }

    rd_ptr = fopen(parsed_args->authfile, "rb");
    if (rd_ptr == NULL) {
        perror("COULD NOT OPEN AUTHFILE");
        exit(255);
    }
    fread(server_publickey, crypto_box_PUBLICKEYBYTES, 1, rd_ptr);
    fread(nonce, crypto_box_NONCEBYTES, 1, rd_ptr);
    fclose(rd_ptr);

    int sock = 0;
    struct sockaddr_in serv_addr;
    char send_buffer[BUFFER_SIZE] = {0};
    char recv_buffer[BUFFER_SIZE] = {0};

    //create TCP socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("SOCKET_CREATION");
        exit(63);
    }

	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
        error("setsockopt failed\n");
	}

    if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
		error("setsockopt failed\n");
	}

    //specify remote server address in struct
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(parsed_args->port);
    if (inet_pton(AF_INET, parsed_args->ip, &serv_addr.sin_addr) <= 0) {
        perror("INET_PTON");
        exit(63);
    }

    if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("CONNECT");
        exit(63);
    }

	sleep(13);

	// doing error for send -1 and < 1 for empty message in one - should actually be two cases
    if (send(sock, client_publickey, crypto_box_PUBLICKEYBYTES, 0) < 1) {
        perror("ERROR WHILE SENDING CLIENT PUBLIC KEY");
        exit(63);
    }
	
	int i;
    if (i = recv(sock, ciphertext, CIPHERTEXT_LEN, 0) < 1) {
        perror("ERROR IN RECEIVING CIPHER TEXT");
        exit(63);
    }

    if (crypto_box_open_easy(plaintext, ciphertext, CIPHERTEXT_LEN, nonce, server_publickey, client_secretkey)) {
        perror("COULD NOT DECRYPT CIPHER TEXT");
        exit(63);
    }

    unsigned char msg[17] = "RECEIVED PUB KEY";
    if (strncmp(plaintext, msg, 17)) {
        perror("MISSING PUB KEY");
        exit(255);
    }
    printf("Public key correctly delivered\n");


    strncpy(plaintext, "SENDING MESSAGE", 16);
    if (crypto_box_easy(ciphertext, plaintext, MESSAGE_LEN, nonce, server_publickey, client_secretkey)) {
        perror("FAILED TO ENCRYPT REQUEST");
        exit(63);
    }

    if (send(sock, ciphertext, CIPHERTEXT_LEN, 0) < 1) {
        perror("ERROR WHILE SENDING REQUEST");
        exit(63);
    }
    bzero(plaintext, MESSAGE_LEN);
    bzero(ciphertext, CIPHERTEXT_LEN);

    if (recv(sock, ciphertext, CIPHERTEXT_LEN, 0) < 1) {
        perror("ERROR WHEN RECEIVING SERVER RESPONSE");
        exit(63);
    }

    if (crypto_box_open_easy(plaintext, ciphertext, CIPHERTEXT_LEN, nonce, server_publickey, client_secretkey)) {
        perror("ERROR WHEN DECRYPTING SERVER RESPONSE");
        exit(63);
    }
    close(sock);
    if (parseResponse(&plaintext) == 0) {
        perror("FAILED TO PARSE THE RESPONSE");
        exit(255);
    }
    exit(0);
}

size_t parseResponse(unsigned char *response) {
    printf("You managed to connect to the bank\n");
    return 1;
}
