#include <stdio.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>

regex_t regex;
int result;

int verify_no_disallowed_name(char *input);

int verify_numeric_input(char *input) {
    if (regcomp(&regex, "[1-9][0-9]*", 0)) {
        fprintf(stderr, "Could not compile regex\n");
        exit(1);
    }
    result = regexec(&regex, input, 0, NULL, 0);
    if (!result || input == 0) {
        return 1;
    }
    return 0;
}

int verify_currency_input(char *input) {
    if (regcomp(&regex, "[0-9]*[\\.][0-9]\\{2\\}$", 0)) {
        fprintf(stderr, "Could not compile regex\n");
        exit(1);
    }
    result = regexec(&regex, input, 0, NULL, 0);
    if (!result) {
        return 1;
    }
    return 0;
}

int verify_file_name_input(char *input) {
    if (regcomp(&regex, "[\\-\\.0-9a-z]\\{1,127\\}$", 0)) {
        fprintf(stderr, "Could not compile regex\n");
        exit(1);
    }
    result = regexec(&regex, input, 0, NULL, 0);
    if (!result) {
        return verify_no_disallowed_name(input);
    }
    return 0;
}

int verify_account_name_input(char *input) {
    if (regcomp(&regex, "[\\-\\.0-9a-z]\\{1,122\\}$", 0)) {
        fprintf(stderr, "Could not compile regex\n");
        exit(1);
    }
    result = regexec(&regex, input, 0, NULL, 0);
    if (!result) {
        return 1;
    }
    return 0;
}

int verify_port_input(char *input) {
    if (verify_numeric_input(input)) {
        int port = atoi(input);
        if (port > 1023 && port < 65535) {
            return 1;
        }
    }
    return 0;
}

int verify_ip_input(char *input) {
    char ip_address[strlen(input)];
    strcpy(ip_address, input);
    char *p = strtok(ip_address, ".");
    int i = 0;
    char *array[5];
    for (int i = 0; i < 5; i++) {
        array[i] = -1;
    }
    while (p != NULL) {
        array[i++] = p;
        p = strtok(NULL, ".");
    }
    if (array[4] != -1) {
        return 0;
    }
    for (int i = 0; i < 4; i++) {
        if (verify_numeric_input(array[i])) {
            int val = atoi(array[i]);
            if (val < 0 || val > 255) {
                return 0;
            }
        } else {
            return 0;
        }
    }
    return 1;
}

int verify_no_disallowed_name(char *input) {
    if (regcomp(&regex, "^\\.\\.$", 0)) {
        fprintf(stderr, "Could not compile regex\n");
        exit(1);
    }
    result = regexec(&regex, input, 0, NULL, 0);
    if (!result) {
        return 0;
    }
    if (regcomp(&regex, "^\\.$", 0)) {
        fprintf(stderr, "Could not compile regex\n");
        exit(1);
    }
    result = regexec(&regex, input, 0, NULL, 0);
    if (!result) {
        return 0;
    }
    return 1;
}

/*
 * Determine whether the input string is a non-negative integer. Returns the extracted integer if possible, -1 if not.
 */
int extract_numeric_value(char *input) {
    if (verify_numeric_input(input)) {
        return atoi(input);
    }
    return -1;
}

float extract_currency_value(char *input) {
    if (verify_currency_input(input)) {
        return atof(input);
    }
    return -1;
}

//TODO: RETURN VALUE SHOULD BE SOMETHING ELSE
char *extract_file_name(char *input) {
    if (verify_file_name_input(input)) {
        return input;
    }
    return -1;
}

char *extract_account_name(char *input) {
    if (verify_account_name_input(input)) {
        return input;
    }
    return -1;
}

int *extract_port(char *input) {
    if (verify_port_input(input)) {
        return atoi(input);
    }
    return -1;
}

char *extract_ip_address(char *input) {
    if (verify_ip_input(input)) {
        return input;
    }
    return -1;
}
