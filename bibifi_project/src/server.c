#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <sodium.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>

#include <netinet/in.h>
#include <netinet/tcp.h>

#include "../include/cmd_line_arg_parser.h"
#include "cmd_line_arg_parser.c"

#define BUFFER_SIZE 1024
#define MAX_PENDING_REQUESTS 3
#define EXIT_STRING "EXIT_SOCKET_CODE"
#define MESSAGE_LEN 1024
#define CIPHERTEXT_LEN (crypto_box_MACBYTES + MESSAGE_LEN)

#define BANK_AUTH_FILE "bank.auth"

size_t parseRequest(unsigned char *plainRequest);
void signal_callback_handler(int signum);
clap_arg_t *parsed_args;
int client_fd;
int main(int argc, char **argv) {

	//handling interupt signal
	signal(SIGINT, signal_callback_handler);

    //Initialize libsodium
    printf("Starting server\n");
    if (sodium_init() < 0) {
        perror("COULD NOT LOAD SODIUM");
        exit(EXIT_FAILURE);
    }

    parsed_args = &(clap_arg_t) {
            .port = 3000,
            .authfile = "bank.auth",
    };

    /* Command-Line-Argument-Parser (CLAP) */
    if (clap_handler(BANK, argc, argv, parsed_args)) {
        perror("FAILED AT PARSING");
        exit(255);
    };
	
    //Server side key generation for crypto box
    unsigned char server_publickey[crypto_box_PUBLICKEYBYTES];
    unsigned char server_secretkey[crypto_box_SECRETKEYBYTES];
    crypto_box_keypair(server_publickey, server_secretkey);

    unsigned char nonce[crypto_box_NONCEBYTES];
    unsigned char ciphertext[CIPHERTEXT_LEN];
    unsigned char plaintext[MESSAGE_LEN];
    randombytes_buf(nonce, sizeof(nonce));

	struct timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    //writing the authfile as binary -> keys are bytes anyway
    printf("Writing authfile\n");
    FILE *write_ptr;
    write_ptr = fopen(parsed_args->authfile, "wb");
    if (write_ptr == NULL) {
        perror("COULD NOT OPEN AUTHFILE");
        exit(255);
    }
    fwrite(server_publickey, sizeof(server_publickey), 1, write_ptr);
    fwrite(nonce, sizeof(nonce), 1, write_ptr);
	
    fclose(write_ptr);

    int server_fd; 
    struct sockaddr_in address;
    //socklen_t sin_size;
    //ssize_t recv_length = 1, yes = 1;
    //int options = 1;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE];

    //create TCP socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("PROTOCOL_ERROR");
        exit(255);
    }

    //specify local port for echo service and accept ANY inconming connection
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(parsed_args->port);
    memset(&(address.sin_zero), '\0', 8);//filling last Byte with zeros

    if (bind(server_fd, (struct sockaddr *) &address, sizeof(address)) == -1) {
        perror("PROTOCOL_ERROR");
        exit(255);
    }
	
    //handle at maximum 3 pending client connection request
    if (listen(server_fd, MAX_PENDING_REQUESTS) < 0) {
        perror("PROTOCOL_ERROR");
        exit(255);
    }

    unsigned char client_publickey[crypto_box_PUBLICKEYBYTES];


    //server runs as long as process is running unless shutdown manually
    while (1) {
        //open new TCP socket for an incoming connection and continue to listen on old port
        if ((client_fd = accept(server_fd, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0) {
            perror("PROTOCOL_ERROR");
        }
        printf("NEW CONNECTION OPENED\n");
        int connectionFinished = 0;
		
		if (setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
			perror("setsockopt failed\n");
		}

    	if (setsockopt(client_fd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
			perror("setsockopt failed\n");
		}

        if (recv(client_fd, client_publickey, crypto_box_PUBLICKEYBYTES, 0) < 1) {
            perror("PROTOCOL_ERROR");
			continue;
        }

        strncpy(plaintext, "RECEIVED PUB KEY", 17);

        if (crypto_box_easy(ciphertext, plaintext, MESSAGE_LEN, nonce, client_publickey,
                            server_secretkey) != 0) {
            perror("PROTOCOL_ERROR");
			continue;
        }
        printf("Sending handshake ciphertext\n");
        //echo back what was sent by client
        if (send(client_fd, ciphertext, CIPHERTEXT_LEN, 0) < 1) {
            perror("PROTOCOL_ERROR");
			continue;
        }
        //zero out the buffer
        bzero(plaintext, MESSAGE_LEN);
        bzero(ciphertext, CIPHERTEXT_LEN);

        if (recv(client_fd, ciphertext, CIPHERTEXT_LEN, 0) < 1) {
            perror("PROTOCOL_ERROR");
			continue;
        }

        if (crypto_box_open_easy(plaintext, ciphertext, CIPHERTEXT_LEN, nonce, client_publickey, server_secretkey) ==
            -1) {
            perror("PROTOCOL_ERROR");
			continue;
        }

        if (parseRequest(&plaintext) == 0) {
            perror("FAILED AT PARSING");
        }

        strncpy(plaintext, "POSITIVE EVALUATION", 20);
        bzero(plaintext, MESSAGE_LEN);
        bzero(ciphertext, CIPHERTEXT_LEN);

        if (crypto_box_easy(ciphertext, plaintext, MESSAGE_LEN, nonce, client_publickey, server_secretkey) == -1) {
            perror("PROTOCOL_ERROR");
        }

        if (send(client_fd, ciphertext, CIPHERTEXT_LEN, 0) < 1) {
            perror("PROTOCOL_ERROR");
        }
    }
}

size_t parseRequest(unsigned char *plainRequest) {
    printf("1\n");
    return 1;
}

void signal_callback_handler(int signum) {
   printf("Abort bank server cought CTRL-C\n");
   remove(parsed_args->authfile);
   exit(signum);
}
