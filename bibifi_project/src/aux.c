#include<linux/if.h>//has to be included for c99 std
#include"../include/aux.h"

void 
dump(const char *data_buffer, const unsigned int length) {
	unsigned char byte;
	unsigned int i,j;

	for(i=0;i < length; i++) {
		byte = data_buffer[i];
		printf("%02x ", data_buffer[i]);
		if( ((i%16) == 15) || (i == length-1)) {
			for(j=0;j < 15 - (i%16); j++)
				printf("  ");
			printf("| ");
			for(j=(i - (i%16)); j <= i; j++) {
				byte = data_buffer[j];
				if((byte > 31) && (byte < 127))
					printf("%c", byte);
				else
					printf(".");
			}
			printf("\n");
		}
	}
}

void 
print_ip(void) {
	char ip_address[15];
    	int fd;
    	struct ifreq ifr;
     
    	//AF_INET - ADDR Family IPv4
    	//AF_INET - to define network interface IPv4
	//Creating soket for it
    	fd = socket(AF_INET, SOCK_DGRAM, 0);
     
    	//AF_INET - ADDR Family IPv4
    	ifr.ifr_addr.sa_family = AF_INET;
     	//eth0 - define the ifr_name for docker - port name igb0 for freeBSD
    	memcpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);//renmae for docker to eth0
     	//Accessing network interface information via address using ioctl
    	ioctl(fd, SIOCGIFADDR, &ifr);
    	/*closing fd*/
    	close(fd);
     
    	/*Extract IP Address*/
    	strcpy(ip_address,inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
     
    	printf("Server IPv4: %s\n", ip_address);
}
