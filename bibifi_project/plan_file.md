# Stuff ToDo until 22.06.2020

### secure function wrapper
 
 * libsafec

### Syscall monitoring with strace
 -> systrace

### Input sanitization 
 
 * black listing
 * white listing
 * convert black list chars to white list chars

### Testing Code regularly via Unit Tests
 
 * gnu autounit?

### Fuzzing CMD arguments 

-> via fuzzer with EBNF?

TODO:

 1. DONE when we close the server lets delete the file
 2. DOne TIMEOUT
 3. parsing error values -> like numerics etc.
 4. accounting
 5. PIN
 6. replay nonce value

